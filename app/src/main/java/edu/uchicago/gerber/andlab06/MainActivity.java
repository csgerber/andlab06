package edu.uchicago.gerber.andlab06;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import edu.uchicago.gerber.andlab06.frags.CountriesFragment;
import edu.uchicago.gerber.andlab06.rest.JsonParser;
import edu.uchicago.gerber.andlab06.rest.gsonjigs.CountryData;
import edu.uchicago.gerber.andlab06.rest.gsonjigs.RestResponse;
import edu.uchicago.gerber.andlab06.rest.gsonjigs.Result;

public class MainActivity extends AppCompatActivity  {


    public static final String HTTP_SERVICES_GROUPKT_COM_COUNTRY_GET_ALL = "http://services.groupkt.com/country/get/all";
    public static final int REQUEST_CODE_FOR_RESULT = 1001;
    private ProgressBar progressBar;
    private Firebase dbReference;
    private List<String> randomFlags;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new BackbroundAsyncTask().execute(HTTP_SERVICES_GROUPKT_COM_COUNTRY_GET_ALL);
            }
        });

        randomFlags = new ArrayList<>();
        randomFlags.add("https://www.cia.gov/library/publications/the-world-factbook/graphics/flags/large/dx-lgflag.gif");
        randomFlags.add("http://2.bp.blogspot.com/-eZSDwQXSQ04/UPiAdHTzMnI/AAAAAAAAcsg/9oBNnV5N2ZU/s1600/south+africa.jpg");
        randomFlags.add("http://d33y93cfm0wb4z.cloudfront.net/Jolanda/Colouring_images/flags/flag%20screenshots/National%20flag%20of%20Scotland.jpg");
        randomFlags.add("http://www.museumsyndicate.com/images/countries/24.png");
        randomFlags.add("https://s-media-cache-ak0.pinimg.com/736x/f9/62/c3/f962c375d17d24423d6186ccff4bdee3.jpg");
        randomFlags.add("http://www.worldatlas.com/webimage/flags/countrys/zzzflags/aularge.gif");
        randomFlags.add("https://s-media-cache-ak0.pinimg.com/236x/db/61/22/db6122528865df01b0c3f0e71c6febf1.jpg");
        randomFlags.add("http://www.free-printable-flags.com/printable-world-flags/europe/hungary.gif");
        randomFlags.add("http://www.yescoloring.com/images/40_Flag_of_Jamaica_at_coloring-pages-book-for-kids-boys.jpg");
        randomFlags.add("http://www.flags.net/images/largeflags/UNKG0102.GIF");
        randomFlags.add("http://www.flags.net/images/largeflags/UNKG7404.GIF");
        randomFlags.add("http://www.flags.net/images/largeflags/UNKG7408.GIF");
        randomFlags.add("http://www.flags.net/images/largeflags/UNKG7413.GIF");
        randomFlags.add("http://www.flags.net/images/largeflags/UNKG7402.GIF");
        randomFlags.add("http://www.flags.net/images/largeflags/UNKG7412.GIF");
        randomFlags.add("http://www.flags.net/images/largeflags/UNKG7415.GIF");

        dbReference = new Firebase("https://andlab10c.firebaseio.com");

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        CountriesFragment countriesFragment = new CountriesFragment();
        // add the fragment to the FrameLayout
        FragmentTransaction transaction =
                getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.fragmentContainer, countriesFragment);
        transaction.addToBackStack(CountriesFragment.class.getName());
        transaction.commit();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (resultCode == Activity.RESULT_OK) {
                switch (requestCode) {
                    case REQUEST_CODE_FOR_RESULT:
                        //fetch the integer we passed into the dialog result which corresponds to the list position
                        Result result = (Result) data.getSerializableExtra(ResultsDialogActivity.MY_RESULT);

                        Random random = new Random();


                        MyCountry myCountry = new MyCountry(result.getName(),
                                randomFlags.get(random.nextInt(randomFlags.size())));

                        dbReference.child("MyCountry").push().setValue(myCountry);


                }
            }
        } catch (Exception e) {
            Toast.makeText(this, "error", Toast.LENGTH_SHORT).show();
        }
    }


    class BackbroundAsyncTask extends AsyncTask<String, Integer, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressBar.setVisibility(View.VISIBLE);


        }


        @Override
        // @UiThread
        protected String doInBackground(String... params) {
            String string = null;

            // progressBar.setProgress(50);


            try {
                string = JsonParser.sendGet(params[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return string;

        }


        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            progressBar.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(String str) {
            super.onPostExecute(str);


            CountryData countryRestData = new Gson().fromJson(str, CountryData.class);
            RestResponse restResponse = countryRestData.getRestResponse();
            ArrayList<Result> mResultList = new ArrayList<>(restResponse.getResult());

            Bundle bundle = new Bundle();
            bundle.putSerializable("simple_data_bundle_key", mResultList);

            Intent intent = new Intent(MainActivity.this, ResultsDialogActivity.class);
            intent.putExtras(bundle);
            MainActivity.this.startActivityForResult(intent, REQUEST_CODE_FOR_RESULT);

            progressBar.setVisibility(View.GONE);

        }


    }//end AsyncTask


    @Override
    public void onBackPressed() {

        FragmentManager fragManager = getSupportFragmentManager();
        if (fragManager.getBackStackEntryCount() <= 1) {
            finish();
        } else {
            super.onBackPressed();
        }

    }
}
