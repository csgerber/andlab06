package edu.uchicago.gerber.andlab06;

import android.app.Application;

import com.firebase.client.Firebase;

/**
 * Created by agerber on 5/27/2016.
 */
public class FlagsOfTheWorld extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Firebase.setAndroidContext(this);
    }
}
