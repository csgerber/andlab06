package edu.uchicago.gerber.andlab06;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

import edu.uchicago.gerber.andlab06.rest.gsonjigs.Result;


public class ResultsDialogActivity extends ListActivity {

    public static final String MY_RESULT = "my_result";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        final ArrayList<Result> resultsList = (ArrayList<Result>) getIntent().getSerializableExtra("simple_data_bundle_key");

        //conver them to strings to display


        ArrayList<String> stringArrayList = new ArrayList<>();
        for (Result res : resultsList) {
            stringArrayList.add(res.getName() + " : " + res.getAlpha3Code());
        }

        ArrayAdapter<String> modeAdapter = new ArrayAdapter<String>(this, R.layout.pop_layout, R.id.pop_text, stringArrayList);



        setListAdapter(modeAdapter);

        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent returnIntent = new Intent();
                returnIntent.putExtra(MY_RESULT, resultsList.get(position));
                setResult(RESULT_OK,returnIntent);
                finish();

            }
        });
    }



}
